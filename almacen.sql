create table Fabricante(
    Codigo INT PRIMARY KEY auto_increment,
    Nombre VARCHAR(30)
);
CREATE TABLE Articulos(
    Codigo INT PRIMARY KEY auto_increment,
    Nombre VARCHAR(30),
    Precio FLOAT(5,2),
    Fabricante INT,
    FOREIGN KEY (Fabricante) REFERENCES Fabricante (Codigo)
);

mysql> select nombre from articulos;
+--------------------+
| nombre             |
+--------------------+
| Martillo           |
| Sierra             |
| Destornillador     |
| Cazadora vaquera   |
| Camiseta           |
| Cinturón           |
| Nintendo Switch    |
| Play Station 4     |
| Xbox One X         |
| Portátil           |
| Patinete eléctrico |
| iPad pro           |
+--------------------+


mysql> select nombre, precio from articulos;
+--------------------+--------+
| nombre             | precio |
+--------------------+--------+
| Martillo           |  12.45 |
| Sierra             |  37.99 |
| Destornillador     |  12.00 |
| Cazadora vaquera   |  29.10 |
| Camiseta           |  12.50 |
| Cinturón           |   8.75 |
| Nintendo Switch    | 299.99 |
| Play Station 4     | 350.00 |
| Xbox One X         | 400.90 |
| Portátil           | 500.00 |
| Patinete eléctrico | 500.00 |
| iPad pro           | 495.95 |
+--------------------+--------+


mysql> select nombre from articulos where precio <= 200;
+------------------+
| nombre           |
+------------------+
| Martillo         |
| Sierra           |
| Destornillador   |
| Cazadora vaquera |
| Camiseta         |
| Cinturón         |
+------------------+


mysql> select * from articulos where precio between 30 and 120;
+--------+--------+--------+------------+
| Codigo | Nombre | Precio | Fabricante |
+--------+--------+--------+------------+
|      2 | Sierra |  37.99 |          1 |
+--------+--------+--------+------------+


mysql> select nombre, precio*166 as PrecioPesetas from articulos;
+--------------------+---------------+
| nombre             | PrecioPesetas |
+--------------------+---------------+
| Martillo           |       2066.70 |
| Sierra             |       6306.34 |
| Destornillador     |       1992.00 |
| Cazadora vaquera   |       4830.60 |
| Camiseta           |       2075.00 |
| Cinturón           |       1452.50 |
| Nintendo Switch    |      49798.34 |
| Play Station 4     |      58100.00 |
| Xbox One X         |      66549.40 |
| Portátil           |      83000.00 |
| Patinete eléctrico |      83000.00 |
| iPad pro           |      82327.70 |
+--------------------+---------------+


mysql> select avg(precio) as PrecioMedio from articulos;
+-------------+
| PrecioMedio |
+-------------+
|  221.635833 |
+-------------+


mysql> select avg(precio) as PrecioMedio from articulos where Fabricante = 2;
+-------------+
| PrecioMedio |
+-------------+
|   16.783333 |
+-------------+


mysql> select count(*) from articulos where precio >= 180;
+----------+
| count(*) |
+----------+
|        6 |
+----------+


mysql> select nombre, precio from articulos where precio >= 180 order by precio DESC, nombre ASC;
+--------------------+--------+
| nombre             | precio |
+--------------------+--------+
| Patinete eléctrico | 500.00 |
| Portátil           | 500.00 |
| iPad pro           | 495.95 |
| Xbox One X         | 400.90 |
| Play Station 4     | 350.00 |
| Nintendo Switch    | 299.99 |
+--------------------+--------+


mysql> select fabricante, avg(precio) as PrecioMedio from articulos group by fabricante;
+------------+-------------+
| fabricante | PrecioMedio |
+------------+-------------+
|          1 |   20.813334 |
|          2 |   16.783333 |
|          3 |  350.296661 |
|          4 |  498.650004 |
+------------+-------------+


select nombre, precio from articulos order by precio limit 1;
+----------+--------+
| nombre   | precio |
+----------+--------+
| Cinturón |   8.75 |
+----------+--------+


mysql> insert into articulos(nombre,precio,fabricante) values ("Altavoces",70,2);
mysql> select * from articulos;
+--------+--------------------+--------+------------+
| Codigo | Nombre             | Precio | Fabricante |
+--------+--------------------+--------+------------+
|      1 | Martillo           |  12.45 |          1 |
|      2 | Sierra             |  37.99 |          1 |
|      3 | Destornillador     |  12.00 |          1 |
|      4 | Cazadora vaquera   |  29.10 |          2 |
|      5 | Camiseta           |  12.50 |          2 |
|      6 | Cinturón           |   8.75 |          2 |
|      7 | Nintendo Switch    | 299.99 |          3 |
|      8 | Play Station 4     | 350.00 |          3 |
|      9 | Xbox One X         | 400.90 |          3 |
|     10 | Portátil           | 500.00 |          4 |
|     11 | Patinete eléctrico | 500.00 |          4 |
|     12 | iPad pro           | 495.95 |          4 |
|     13 | Altavoces          |  70.00 |          2 |
+--------+--------------------+--------+------------+


mysql> update articulos set nombre = "Impresora láser" where codigo = 8;
mysql> select * from articulos;
+--------+--------------------+--------+------------+
| Codigo | Nombre             | Precio | Fabricante |
+--------+--------------------+--------+------------+
|      1 | Martillo           |  12.45 |          1 |
|      2 | Sierra             |  37.99 |          1 |
|      3 | Destornillador     |  12.00 |          1 |
|      4 | Cazadora vaquera   |  29.10 |          2 |
|      5 | Camiseta           |  12.50 |          2 |
|      6 | Cinturón           |   8.75 |          2 |
|      7 | Nintendo Switch    | 299.99 |          3 |
|      8 | Impresora láser    | 350.00 |          3 |
|      9 | Xbox One X         | 400.90 |          3 |
|     10 | Portátil           | 500.00 |          4 |
|     11 | Patinete eléctrico | 500.00 |          4 |
|     12 | iPad pro           | 495.95 |          4 |
|     13 | Altavoces          |  70.00 |          2 |
+--------+--------------------+--------+------------+


mysql> update articulos set precio = precio*0.9;
mysql> select * from articulos;
+--------+--------------------+--------+------------+
| Codigo | Nombre             | Precio | Fabricante |
+--------+--------------------+--------+------------+
|      1 | Martillo           |  11.20 |          1 |
|      2 | Sierra             |  34.19 |          1 |
|      3 | Destornillador     |  10.80 |          1 |
|      4 | Cazadora vaquera   |  26.19 |          2 |
|      5 | Camiseta           |  11.25 |          2 |
|      6 | Cinturón           |   7.88 |          2 |
|      7 | Nintendo Switch    | 269.99 |          3 |
|      8 | Impresora láser    | 315.00 |          3 |
|      9 | Xbox One X         | 360.81 |          3 |
|     10 | Portátil           | 450.00 |          4 |
|     11 | Patinete eléctrico | 450.00 |          4 |
|     12 | iPad pro           | 446.36 |          4 |
|     13 | Altavoces          |  63.00 |          2 |
+--------+--------------------+--------+------------+


mysql> update articulos set precio = precio-10 where precio >= 120;
mysql> select * from articulos;
+--------+--------------------+--------+------------+
| Codigo | Nombre             | Precio | Fabricante |
+--------+--------------------+--------+------------+
|      1 | Martillo           |  11.20 |          1 |
|      2 | Sierra             |  34.19 |          1 |
|      3 | Destornillador     |  10.80 |          1 |
|      4 | Cazadora vaquera   |  26.19 |          2 |
|      5 | Camiseta           |  11.25 |          2 |
|      6 | Cinturón           |   7.88 |          2 |
|      7 | Nintendo Switch    | 259.99 |          3 |
|      8 | Impresora láser    | 305.00 |          3 |
|      9 | Xbox One X         | 350.81 |          3 |
|     10 | Portátil           | 440.00 |          4 |
|     11 | Patinete eléctrico | 440.00 |          4 |
|     12 | iPad pro           | 436.36 |          4 |
|     13 | Altavoces          |  63.00 |          2 |
+--------+--------------------+--------+------------+