CREATE database proyectos;
CREATE TABLE cientificos(
    DNI VARCHAR(8) primary key,
    NomApels VARCHAR(255)
);
CREATE TABLE proyecto(
    Id CHAR(4) primary key,
    Nombre VARCHAR(255),
    Horas INT
);
CREATE TABLE asignado(
    Cientifico VARCHAR(8),
    Proyecto CHAR(4)
);

INSERT INTO cientificos (DNI,NomApels) 
VALUES ("63718641","Keith Zimmerman"),
("02364630","Barclay Cross"),
("00888686","Kennan Ray"),
("47886741","Wesley Hartman"),
("58451967","Flynn Hensley"),
("68791217","Timon Stout"),
("49270301","Bruce Finch"),
("19926611","Igor Kim"),
("00076221","Neil Haney"),
("88177746","Fitzgerald Mcguire");

INSERT INTO proyecto (Id,Nombre,Horas) 
VALUES (1,"php",33),
(2,"js",97),
(3,"node",82),
(4,"sql",69),
(5,"mongo",56),
(6,"html",49),
(7,"css",67);

INSERT INTO asignado (Cientifico, Proyecto)
VALUES ("68791217", 6),
("68791217", 2),
("47886741", 1),
("00076221", 1),
("88177746", 5),
("49270301", 3);

1.-Sacar una relación completa de los científicos asignados a cada proyecto. 
Mostrar DNI, Nombre del científico, Identificador del proyecto y nombre del proyecto.

select c.DNI, c.NomApels, p.Id, p.Nombre 
from cientificos c 
join asignado a
on c.DNI = a.Cientifico
join proyecto p
on a.Proyecto = p.Id
group by p.Id;

+----------+--------------------+----+--------+
| DNI      | NomApels           | Id | Nombre |
+----------+--------------------+----+--------+
| 68791217 | Timon Stout        | 6  | html   |
| 68791217 | Timon Stout        | 2  | js     |
| 47886741 | Wesley Hartman     | 1  | php    |
| 88177746 | Fitzgerald Mcguire | 5  | mongo  |
| 49270301 | Bruce Finch        | 3  | node   |
+----------+--------------------+----+--------+


2.-Obtener  el  número  de  proyectos  al  que  está asignado  cada  científico 
(mostrar  el DNI y el nombre).

select count(id) as numProyectosAsignados, c.dni, c.nomapels 
from cientificos c 
join asignado a on c.dni = a.cientifico 
join proyecto p on a.proyecto = p.id 
group by c.dni;
+-----------------------+----------+--------------------+
| numProyectosAsignados | dni      | nomapels           |
+-----------------------+----------+--------------------+
|                     2 | 68791217 | Timon Stout        |
|                     1 | 47886741 | Wesley Hartman     |
|                     1 | 00076221 | Neil Haney         |
|                     1 | 88177746 | Fitzgerald Mcguire |
|                     1 | 49270301 | Bruce Finch        |
+-----------------------+----------+--------------------+


3.-Obtener el número de científicos asignados a cada proyecto 
(mostrar el identificador de proyecto y el nombre del proyecto).

select count(dni) as numCientificosAsignados, p.id, p.nombre 
from proyecto p 
join asignado a on a.proyecto = p.id 
join cientificos c on c.dni = a.cientifico 
group by p.id;

+-------------------------+----+--------+
| numCientificosAsignados | id | nombre |
+-------------------------+----+--------+
|                       1 | 6  | html   |
|                       1 | 2  | js     |
|                       2 | 1  | php    |
|                       1 | 5  | mongo  |
|                       1 | 3  | node   |
+-------------------------+----+--------+


4.-Obtener el número de horas de dedicación de cada científico.

select c.nomapels, sum(horas) 
from cientificos c 
join asignado a on c.dni = a.cientifico 
join proyecto p on a.proyecto = p.id 
group by c.dni;
+--------------------+------------+
| nomapels           | sum(horas) |
+--------------------+------------+
| Timon Stout        |        146 |
| Wesley Hartman     |         33 |
| Neil Haney         |         33 |
| Fitzgerald Mcguire |         56 |
| Bruce Finch        |         82 |
+--------------------+------------+


5.-Obtener el DNI y nombre de los científicos que se dedican a más de un proyecto y cuya dedicación 
media a cada proyecto sea superior a las 70 horas.

select c.dni, c.nomapels 
from cientificos c 
join asignado a on c.dni = a.cientifico 
join proyecto p on a.proyecto = p.id 
where c.dni = (select dni 
               from cientificos c 
               join asignado a on c.dni = a.cientifico 
               join proyecto p on a.proyecto = p.id 
               group by c.dni 
               having count(id) > 1) 
and c.nomapels=(select c.nomapels 
                from cientificos c 
                join asignado a on c.dni = a.cientifico 
                join proyecto p on a.proyecto = p.id 
                group by c.dni 
                having (sum(horas)/2)> 70) 
group by c.dni;

+----------+-------------+
| dni      | nomapels    |
+----------+-------------+
| 68791217 | Timon Stout |
+----------+-------------+