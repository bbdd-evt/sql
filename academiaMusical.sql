CREATE TABLE socios (
Documento VARCHAR (9) primary key,
Nombre VARCHAR (200),
Residencia VARCHAR (300)
);
CREATE TABLE inscritos (
Asignatura VARCHAR(250) primary key,
Año YEAR,
CuotaPagada enum('Si','No'),
DocumentoSocio VARCHAR(9),
foreign key(DocumentoSocio) references socios (Documento)
);

1. Muestre el nombre del socio y todos las lecciones a las que esté inscrito (con todos sus atributos)

select s.Nombre, i.Asignatura, i.Año, i.CuotaPagada 
from socios s join inscritos i 
on s.Documento = i.DocumentoSocio;
+--------------------------+-------------+------+-------------+
| Nombre                   | Asignatura  | Año  | CuotaPagada |
+--------------------------+-------------+------+-------------+
| Guillermo Montero Martín | Piano       | 2017 | Si          |
| Eduardo Vidal Tabernero  | Composición | 2019 | Si          |
| Eduardo Vidal Tabernero  | Solfeo      | 2018 | No          |
| Cesarin Minyetty         | Trompeta    | 2019 | Si          |
+--------------------------+-------------+------+-------------+


2. Muestre el nombre de los socios y las asignaturas en los cuales están inscritos en el año 2019.

select s.Nombre, i.Asignatura, i.Año 
from socios s join inscritos i 
on s.Documento = i.DocumentoSocio 
where i.Año = 2019;
+-------------------------+-------------+------+
| Nombre                  | Asignatura  | Año  |
+-------------------------+-------------+------+
| Eduardo Vidal Tabernero | Composición | 2019 |
| Cesarin Minyetty        | Trompeta    | 2019 |
+-------------------------+-------------+------+


3. Muestre el nombre y toda la información de las inscripciones del socio con número de documento='50909350T'.

select s.Nombre, i.* 
from socios s join inscritos i 
on s.Documento=i.DocumentoSocio 
where i.DocumentoSocio = '50909350T';
+-------------------------+-------------+------+-------------+----------------+
| Nombre                  | Asignatura  | Año  | CuotaPagada | DocumentoSocio |
+-------------------------+-------------+------+-------------+----------------+
| Eduardo Vidal Tabernero | Composición | 2019 | Si          | 50909350T      |
| Eduardo Vidal Tabernero | Solfeo      | 2018 | No          | 50909350T      |
+-------------------------+-------------+------+-------------+----------------+