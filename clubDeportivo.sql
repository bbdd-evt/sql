create database centroDeportivo;
use centroDeportivo;
create table PROVEEDOR(
 idProveedor INT primary key auto_increment,
 nombreEmpresa varchar (150),
 direccion varchar (250),
 poblacion varchar (100),
 provincia varchar (100),
 codPostal varchar (10),
 personaContacto varchar (150),
 telefono varchar (9),
 email varchar (100)
);
create table USUARIO(
 numUsuario INT PRIMARY KEY auto_increment,
 fechaAlta TIMESTAMP,
 nombre VARCHAR(70),
 apellidos VARCHAR(150),
 fechaNacimiento DATE,
 DNI VARCHAR(9),
 telefono VARCHAR(12),
 email VARCHAR(100),
 direccion VARCHAR(250),
 poblacion VARCHAR(100),
 provincia VARCHAR(100),
 codPostal VARCHAR(10),
 cuentaCorriente VARCHAR(200)
);
create table ADMINISTRACION(
 idAdministracion INT PRIMARY KEY auto_increment,
 responsable VARCHAR(200),
 horario VARCHAR(200),
 direccion VARCHAR(250),
 telefono VARCHAR(9),
 email VARCHAR(100),
 idProveedor int,
 numUsuario int,
 FOREIGN KEY (idProveedor) REFERENCES PROVEEDOR (idProveedor),
 FOREIGN KEY (numUsuario) REFERENCES USUARIO (numUsuario)
);
create table ACTIVIDAD(
 codActividad INT PRIMARY KEY auto_increment,
 horario VARCHAR(200),
 instalacion VARCHAR(200)
);
create table EMPLEADO(
 idEmpleado INT PRIMARY KEY auto_increment,
 especialidad VARCHAR(200),
 nombre VARCHAR(70),
 apellidos VARCHAR(150),
 DNI VARCHAR(9),
 telefono VARCHAR(12),
 email VARCHAR(100),
 direccion VARCHAR(250),
 poblacion VARCHAR(100),
 provincia VARCHAR(100),
 codPostal VARCHAR(10),
 cuentaCorriente VARCHAR(200)
);
create table USUARIOACTIVIDAD(
 numUsuario INT,
 codActividad INT,
 fecha TIMESTAMP,
 foreign key(numUsuario) references USUARIO (numUsuario),
 foreign key(codActividad) references ACTIVIDAD (codActividad),
 primary key(numUsuario, codActividad)
);
create table EMPLEADOACTIVIDAD(
 idEmpleado INT,
 codActividad INT,
 fecha TIMESTAMP,
 foreign key(idEmpleado) references EMPLEADO (idEmpleado),
 foreign key(codActividad) references ACTIVIDAD (codActividad),
 primary key(idEmpleado, codActividad)
);

/*CAMBIOS*/
 alter table actividad modify column instalacion varchar(200) after codActividad;
 alter table empleado modify especialidad varchar(170);
 alter table usuario drop telefono;
 alter table proveedor change provincia prov varchar(120);