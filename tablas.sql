CREATE TABLE libro (
ISBN VARCHAR(13) primary key,
Titulo VARCHAR (100),
Genero VARCHAR (50),
Autor VARCHAR (60),
NumPaginas INT,
Precio FLOAT(5,2),
Sinopsis VARCHAR(500)
);
CREATE TABLE comprador (
CodComprador INT primary key auto_increment,
Nombre VARCHAR (60),
DNI VARCHAR (9),
FechaNacimiento DATE,
Email VARCHAR (70),
ISBNComprado VARCHAR (13),
foreign key(ISBNComprado) references libro (ISBN)
);
CREATE TABLE editorial (
CodEditorial INT primary key auto_increment,
Nombre VARCHAR (60),
PersonaContacto VARCHAR (50),
Telefono VARCHAR(12)
);
CREATE TABLE libro_editorial (
ISBNLibro VARCHAR(13),
CodEditorialLibro INT,
FechaPedido DATE,
foreign key(ISBNLibro) references libro (ISBN),
foreign key(CodEditorialLibro) references editorial (CodEditorial),
primary key(ISBNLibro, CodEditorialLibro)
);
CREATE TABLE pedido (
CodPedido INT primary key auto_increment,
Fecha DATE,
ISBNPedido VARCHAR(13),
CodCompradorPedido INT,
foreign key(ISBNPedido) references libro (ISBN),
foreign key(CodCompradorPedido) references comprador (CodComprador)
);
CREATE TABLE factura (
CodFactura INT primary key auto_increment,
Fecha DATE,
Total FLOAT (7,2),
CodPedidoFactura INT,
foreign key(CodPedidoFactura) references pedido (CodPedido)
);
