1. Obtener un listado completo de artículos, incluyendo por cada artículo los datos del artículo y de su fabricante.

select * from articulos a,fabricante f where a.fabricante = f.codigo;
+--------+--------------------+--------+------------+--------+-----------+
| Codigo | Nombre             | Precio | Fabricante | Codigo | Nombre    |
+--------+--------------------+--------+------------+--------+-----------+
|      1 | Martillo           |  11.20 |          1 |      1 | Eduardo   |
|      2 | Sierra             |  34.19 |          1 |      1 | Eduardo   |
|      3 | Destornillador     |  10.80 |          1 |      1 | Eduardo   |
|      4 | Cazadora vaquera   |  26.19 |          2 |      2 | Guillermo |
|      5 | Camiseta           |  11.25 |          2 |      2 | Guillermo |
|      6 | Cinturón           |   7.88 |          2 |      2 | Guillermo |
|     13 | Altavoces          |  63.00 |          2 |      2 | Guillermo |
|      7 | Nintendo Switch    | 259.99 |          3 |      3 | Cesarin   |
|      8 | Impresora láser    | 305.00 |          3 |      3 | Cesarin   |
|      9 | Xbox One X         | 350.81 |          3 |      3 | Cesarin   |
|     10 | Portátil           | 440.00 |          4 |      4 | Yves      |
|     11 | Patinete eléctrico | 440.00 |          4 |      4 | Yves      |
|     12 | iPad pro           | 436.36 |          4 |      4 | Yves      |
+--------+--------------------+--------+------------+--------+-----------+


2. Obtener un listado de artículos, incluyendo el nombre del artículo, su precio, y el nombre de su fabricante.

select a.nombre, a.precio, f.nombre as nombreFabricante from articulos a, fabricante f where a.fabricante = f.codigo;
+--------------------+--------+------------------+
| nombre             | precio | nombreFabricante |
+--------------------+--------+------------------+
| Martillo           |  11.20 | Eduardo          |
| Sierra             |  34.19 | Eduardo          |
| Destornillador     |  10.80 | Eduardo          |
| Cazadora vaquera   |  26.19 | Guillermo        |
| Camiseta           |  11.25 | Guillermo        |
| Cinturón           |   7.88 | Guillermo        |
| Altavoces          |  63.00 | Guillermo        |
| Nintendo Switch    | 259.99 | Cesarin          |
| Impresora láser    | 305.00 | Cesarin          |
| Xbox One X         | 350.81 | Cesarin          |
| Portátil           | 440.00 | Yves             |
| Patinete eléctrico | 440.00 | Yves             |
| iPad pro           | 436.36 | Yves             |
+--------------------+--------+------------------+


3. Obtener el precio medio de los productos de cada fabricante, mostrando solo los códigos de fabricante.

select f.codigo, truncate(avg(precio),2) as media from fabricante f, articulos a where a.fabricante = f.codigo group by f.codigo;
+--------+--------+
| codigo | media  |
+--------+--------+
|      1 |  18.72 |
|      2 |  27.08 |
|      3 | 305.26 |
|      4 | 438.78 |
+--------+--------+


4. Obtener el precio medio de los productos de cada fabricante, mostrando el nombre del fabricante.

select f.nombre, truncate(avg(precio),2) as media from fabricante f, articulos a where a.fabricante = f.codigo group by f.codigo;
+-----------+-------------+
| nombre    | media       |
+-----------+-------------+
| Eduardo   |   18.72     |
| Guillermo |   27.08     |
| Cesarin   |  305.26     |
| Yves      |  438.78     |
+-----------+-------------+


5. Obtener los nombres de los fabricantes que ofrezcan productos cuyo precio medio sea mayor o igual a 150€

select f.nombre, truncate(avg(precio),2) as precioMedio from fabricante f, articulos a where a.fabricante = f.codigo group by f.codigo having(avg(precio))>=150;
+---------+-------------+
| nombre  | precioMedio |
+---------+-------------+
| Cesarin |  305.26     |
| Yves    |  438.78     |
+---------+-------------+


6. Obtener el nombre y precio del artículo ḿas barato.

select nombre, precio from articulos where precio = (select min(precio) from articulos);
+----------+--------+
| nombre   | precio |
+----------+--------+
| Cinturón |   7.88 |
+----------+--------+


7. Obtener una lista con el nombre y precio de los artículos más caros de cada proveedor (incluyendo el nombre del proveedor).

select a.nombre,max(precio) as PrecioMaximo ,f.nombre as nombreFabricante from articulos a, fabricante f where a.fabricante=f.codigo group by fabricante;
+------------------+--------------+------------------+
| nombre           | PrecioMaximo | nombreFabricante |
+------------------+--------------+------------------+
| Martillo         |       34.19  | Eduardo          |
| Cazadora vaquera |       63.00  | Guillermo        |
| Nintendo Switch  |      350.81  | Cesarin          |
| Portátil         |      440.00  | Yves             |
+------------------+--------------+------------------+