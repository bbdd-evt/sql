1. Obtener los apellidos de los empleados.

select apellidos 
from empleados;
+----------------------+
| apellidos            |
+----------------------+
| Escarcha Martín      |
| Varela Gómez         |
| Montero Martín       |
| Montero Martín       |
| Sacristán Ortega     |
| Fernández Larios     |
| Martínez San Segundo |
| Menéndez Castro      |
| Sánchez Baranda      |
| Iglesias Junior      |
| Quesada Cervantes    |
| Esteban Sánchez      |
| Rodríguez Maroto     |
+----------------------+


2. Obtener los apellidos de los empleados sin repeticiones

select distinct apellidos 
from empleados;
+----------------------+
| apellidos            |
+----------------------+
| Escarcha Martín      |
| Varela Gómez         |
| Montero Martín       |
| Sacristán Ortega     |
| Fernández Larios     |
| Martínez San Segundo |
| Menéndez Castro      |
| Sánchez Baranda      |
| Iglesias Junior      |
| Quesada Cervantes    |
| Esteban Sánchez      |
| Rodríguez Maroto     |
+----------------------+


3. Obtener todos los datos de los empleados  que se apellidan 'Martín'.

select * 
from empleados 
where apellidos like "%Montero%";
+-----------+-----------+----------------------+------+
| DNI       | nombre    | apellidos            | dpto |
+-----------+-----------+----------------------+------+
| 70022425D | Jorge     | Montero Martín       |    4 |
| 70023425D | Guillermo | Montero Martín       |    4 |
+-----------+-----------+----------------------+------+


4. Obtener todos los datos de los empleados que  se  apellidan  'Baranda'  y  los que se apellidan 'Esteban'.

select * 
from empleados 
where apellidos like '%Baranda%' or apellidos like '%Esteban%';
+-----------+---------+-----------------+------+
| DNI       | nombre  | apellidos       | dpto |
+-----------+---------+-----------------+------+
| 70076523G | Irene   | Sánchez Baranda |    4 |
| 70087182F | Alberto | Esteban Sánchez |    1 |
+-----------+---------+-----------------+------+

5. Obtener todos los datos de los empleados que trabajan para el departamento 2.

select e.* 
from empleados e join departamentos d 
on e.dpto = d.codigo 
where d.codigo = 2;
+-----------+--------+------------------+------+
| DNI       | nombre | apellidos        | dpto |
+-----------+--------+------------------+------+
| 70076192F | Ylenia | Menéndez Castro  |    2 |
| 70078339L | Aroa   | Iglesias Junior  |    2 |
| 70099012F | David  | Rodríguez Maroto |    2 |
+-----------+--------+------------------+------+


6. Obtener todos los datos de los empleados que trabajan para el departamento 3 y para el departamento 4.

select e.* 
from empleados e join departamentos d 
on e.dpto = d.codigo 
where d.codigo = 3 or d.codigo = 4;
+-----------+-----------+----------------------+------+
| DNI       | nombre    | apellidos            | dpto |
+-----------+-----------+----------------------+------+
| 70002889S | María     | Escarcha Martín      |    3 |
| 70022425D | Jorge     | Montero Martín       |    4 |
| 70023425D | Guillermo | Montero Martín       |    4 |
| 70029873H | Roberto   | Fernández Larios     |    3 |
| 70055662X | Ana       | Martínez San Segundo |    3 |
| 70076523G | Irene     | Sánchez Baranda      |    4 |
+-----------+-----------+----------------------+------+


7. Obtener todos los datos de los empleados cuyo apellido comience por ’P’.

select e.* 
from empleados e 
where e.apellidos like 'M%';
+-----------+-----------+----------------------+------+
| DNI       | nombre    | apellidos            | dpto |
+-----------+-----------+----------------------+------+
| 70022425D | Jorge     | Montero Martín       |    4 |
| 70023425D | Guillermo | Montero Martín       |    4 |
| 70055662X | Ana       | Martínez San Segundo |    3 |
| 70076192F | Ylenia    | Menéndez Castro      |    2 |
+-----------+-----------+----------------------+------+


8. Obtener el presupuesto total de todos los departamentos.

select sum(presupuesto) as totalPresupuesto 
from departamentos;
+------------------+
| totalPresupuesto |
+------------------+
|        700000000 |
+------------------+


9. Obtener el número de empleados en cada departamento.

select count(*) as totalEmpleados,d.nombre 
from empleados e join departamentos d 
on e.dpto = d.codigo 
group by d.codigo;
+----------------+----------------+
| totalEmpleados | nombre         |
+----------------+----------------+
|              3 | Creatividad    |
|              4 | Ventas         |
|              3 | Administración |
|              3 | Marketing      |
+----------------+----------------+


10. Obtener  un listado completo de empleados, incluyendo por cada empleado los datos del empleado y de su departamento

select * 
from empleados e join departamentos d 
on e.dpto = d.codigo;
+-----------+-----------+----------------------+------+--------+----------------+-------------+
| DNI       | nombre    | apellidos            | dpto | codigo | nombre         | presupuesto |
+-----------+-----------+----------------------+------+--------+----------------+-------------+
| 70002889S | María     | Escarcha Martín      |    3 |      3 | Creatividad    |   100000000 |
| 70009126G | Marta     | Varela Gómez         |    1 |      1 | Ventas         |   100000000 |
| 70023425D | Guillermo | Montero Martín       |    4 |      4 | Administración |   300000000 |
| 70023991R | Jorge     | Sacristán Ortega     |    1 |      1 | Ventas         |   100000000 |
| 70029873H | Roberto   | Fernández Larios     |    3 |      3 | Creatividad    |   100000000 |
| 70055662X | Ana       | Martínez San Segundo |    3 |      3 | Creatividad    |   100000000 |
| 70076192F | Ylenia    | Menéndez Castro      |    2 |      2 | Marketing      |   200000000 |
| 70076523G | Irene     | Sánchez Baranda      |    4 |      4 | Administración |   300000000 |
| 70078339L | Aroa      | Iglesias Junior      |    2 |      2 | Marketing      |   200000000 |
| 70081662D | Mario     | Quesada Cervantes    |    1 |      1 | Ventas         |   100000000 |
| 70087182F | Alberto   | Esteban Sánchez      |    1 |      1 | Ventas         |   100000000 |
| 70099012F | David     | Rodríguez Maroto     |    2 |      2 | Marketing      |   200000000 |
+-----------+-----------+----------------------+------+--------+----------------+-------------+


11. Obtener un listado completo de empleados, incluyendo el nombre y apellidos del empleado junto al nombre y presupuesto de su departamento.

select e.nombre, e.apellidos, d.nombre, d.presupuesto 
from empleados e join departamentos d 
on e.dpto = d.codigo;
+-----------+----------------------+----------------+-------------+
| nombre    | apellidos            | nombre         | presupuesto |
+-----------+----------------------+----------------+-------------+
| María     | Escarcha Martín      | Creatividad    |   100000000 |
| Marta     | Varela Gómez         | Ventas         |   100000000 |
| Jorge     | Montero Martín       | Administración |   300000000 |
| Guillermo | Montero Martín       | Administración |   300000000 |
| Jorge     | Sacristán Ortega     | Ventas         |   100000000 |
| Roberto   | Fernández Larios     | Creatividad    |   100000000 |
| Ana       | Martínez San Segundo | Creatividad    |   100000000 |
| Ylenia    | Menéndez Castro      | Marketing      |   200000000 |
| Irene     | Sánchez Baranda      | Administración |   300000000 |
| Aroa      | Iglesias Junior      | Marketing      |   200000000 |
| Mario     | Quesada Cervantes    | Ventas         |   100000000 |
| Alberto   | Esteban Sánchez      | Ventas         |   100000000 |
| David     | Rodríguez Maroto     | Marketing      |   200000000 |
+-----------+----------------------+----------------+-------------+


12. Obtener los nombres y apellidos de los empleados que trabajen en departamentos cuyo presupuesto sea mayor de 100.000.000€ .

select e.nombre, e.apellidos, d.nombre, d.presupuesto 
from empleados e join departamentos d 
on e.dpto = d.codigo 
where d.presupuesto > 100000000;
+-----------+------------------+----------------+-------------+
| nombre    | apellidos        | nombre         | presupuesto |
+-----------+------------------+----------------+-------------+
| Jorge     | Montero Martín   | Administración |   300000000 |
| Guillermo | Montero Martín   | Administración |   300000000 |
| Ylenia    | Menéndez Castro  | Marketing      |   200000000 |
| Irene     | Sánchez Baranda  | Administración |   300000000 |
| Aroa      | Iglesias Junior  | Marketing      |   200000000 |
| David     | Rodríguez Maroto | Marketing      |   200000000 |
+-----------+------------------+----------------+-------------+


13. Obtener los datos de los departamentos cuyo presupuesto es superior al presupuesto medio de todos los departamentos.

select * 
from departamentos 
where presupuesto > (select avg(presupuesto) from departamentos) 
group by codigo;
+--------+----------------+-------------+
| codigo | nombre         | presupuesto |
+--------+----------------+-------------+
|      2 | Marketing      |   200000000 |
|      4 | Administración |   300000000 |
+--------+----------------+-------------+


14. Obtener los nombres (únicamente los nombres) de los departamentos que tienen más de tres empleados.

select d.nombre 
from departamentos d join empleados e 
on e.dpto = d.codigo 
group by d.codigo 
having count(e.nombre) > 3;
+--------+
| nombre |
+--------+
| Ventas |
+--------+


15. Añadir un nuevo departamento: ‘Calidad’, con presupuesto de 40.000€ y código 11.
Añadir un empleado vinculado al departamento recién creado: Esther Vázquez, DNI: 89267109

insert into departamentos values (000011, 'Calidad', 40000);
insert into empleados values ('89267109R', 'Esther', 'Vázquez', 000011);


16. Aplicar un recorte presupuestario del 10 % a todos los departamentos.

update departamentos set presupuesto = presupuesto*0.9;


17. Reasignar a los empleados del departamento 1 al departamento 11.

update empleados set dpto = 11 where dpto = 1;


18. Despedir a todos los empleados que trabajan para el departamento 2

 delete from empleados where dpto = 2;


19. Despedir a todos los empleados que trabajen para departamentos cuyo presupuesto sea superior a los 60.000 € .

delete e from empleados e join departamentos d on e.dpto = d.codigo where d.presupuesto > 60000;


20. Se cierra la empresa. Despedir a todos los empleados.

delete from empleados;